# v1.1.3 (unreleased)

  * Fix test suite to not rely on `io::Error`'s `Debug` output.

# v1.1.2

  * Dependency specifications for `serde_json`.

# v1.1.1

  * Dependency specifications for `git-workarea` and `error-chain` have been
    relaxed.

# v1.1.0

  * `utils::write_job` now waits for the job file to have been processed
    before returning.
  * At least `serde_json-0.9.8` is now required for the `entry` method.

# v1.0.1

  * Archives are now written using RFC3339. This removes the spaces that were
    previously there.
  * Jobs are added to the archive properly now as well.

# v1.0.0

  * Initial stable release.

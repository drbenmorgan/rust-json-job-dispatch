// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::inotify::{Events, Inotify, WatchMask};

use std::io::Result;
use std::path::Path;

/// A watcher of a directory for new files.
pub struct Watcher {
    /// The internal inotify watcher.
    ino: Inotify,
    /// The internal buffer for event storage.
    buffer: [u8; 1024],
}

impl Watcher {
    /// Watch a given directory.
    pub fn new(path: &Path) -> Result<Self> {
        let mut ino = Inotify::init()?;
        ino.add_watch(path,
                      WatchMask::CLOSE_WRITE | WatchMask::MOVED_TO | WatchMask::ONLYDIR)?;

        Ok(Self {
            ino: ino,
            buffer: [0; 1024],
        })
    }

    /// Block until events are returned.
    pub fn events(&mut self) -> Result<Events> {
        Ok(self.ino.read_events_blocking(&mut self.buffer)?)
    }
}
